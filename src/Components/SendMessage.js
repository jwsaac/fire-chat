import { Button, Input } from '@material-ui/core';
import firebase from 'firebase';
import { db, auth } from '../firebase';
import React, { useState } from 'react';

const SendMessage = () => {
  const [message, setMessage] = useState('');
  const Send = async (event) => {
    event.preventDefault();
    const { photoURL, uid } = auth.currentUser;

    await db.collection('messages').add({
      uid: uid,
      photoURL: photoURL,
      Text: message,
      CreatedAt: firebase.firestore.FieldValue.serverTimestamp(),
    });

    setMessage('');
  };
  return (
    <div>
      <form onSubmit={Send}>
        <div className='sendMsg'>
          <Input
            style={{
              width: '78%',
              fontSize: '15px',
              fontWeight: '550',
              marginLeft: '5px',
              marginBottom: '-3px',
            }}
            value={message}
            onChange={(event) => setMessage(event.target.value)}
            placeholder='Message...'
          />
          <Button
            style={{
              width: '18%',
              fontSize: '15px',
              fontWeight: '550',
              margin: '4px 5% -13px 5%',
              maxWidth: '200px',
            }}
            type='submit'
          >
            Send
          </Button>
        </div>
      </form>
    </div>
  );
};

export default SendMessage;
