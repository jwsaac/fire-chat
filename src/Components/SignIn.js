import React from 'react';
import { auth } from '../firebase';
import firebase from 'firebase';
import { Button } from '@material-ui/core';

const SignIn = () => {
  const SignInWithGoogle = () => {
    //console.log(auth);
    const provider = new firebase.auth.GoogleAuthProvider();
    auth.signInWithPopup(provider);
  };
  return (
    <div>
      <Button onClick={SignInWithGoogle}>Sign In With Google</Button>
    </div>
  );
};

export default SignIn;
