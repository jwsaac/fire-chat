import React from 'react';
import { Button } from '@material-ui/core';
import { auth } from '../firebase.js';

const SignOut = () => {
  return (
    <div>
      <Button onClick={() => auth.signOut()}>SignOut</Button>
    </div>
  );
};

export default SignOut;
