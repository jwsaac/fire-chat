import React, { useState, useEffect } from 'react';
import { db, auth } from '../firebase';
import SendMessage from './SendMessage';
import SignOut from './SignOut';

const Chat = () => {
  const [messages, setMessages] = useState([]);
  useEffect(() => {
    db.collection('messages')
      .orderBy('CreatedAt')
      .limit(50)
      .onSnapshot((snapshot) => {
        //setMessages(snapshot.docs.map((doc) => doc.data()));
        setMessages(
          snapshot.docs.map((doc) => {
            return { ...doc.data(), id: doc.id };
          })
        );
      });
    console.log('Inside useEffect');
  }, []);

  //console.log(messages);

  return (
    <div>
      <SignOut />
      <div className='msgs'>
        {messages.map((message) => (
          <div>
            <div
              key={message.id}
              className={`msg ${
                message.uid === auth.currentUser.uid ? 'sent' : 'received'
              }`}
            >
              <img src={message.photoURL} alt='' />
              <p>{message.Text}</p>
            </div>
          </div>
        ))}
      </div>
      <SendMessage />
    </div>
  );
};

export default Chat;
