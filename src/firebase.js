import firebase from 'firebase';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyBrLUr9F-QmSKSfvviV6kZvRJz3KpHcHco',
  authDomain: 'fire-chat-f73af.firebaseapp.com',
  projectId: 'fire-chat-f73af',
  storageBucket: 'fire-chat-f73af.appspot.com',
  messagingSenderId: '1099088190020',
  appId: '1:1099088190020:web:eb17fba9f5e9572382ba11',
  measurementId: 'G-9CF60L3F34',
};

var firebaseApp = null;

if (!firebase.apps.length) {
  firebaseApp = firebase.initializeApp(firebaseConfig);
} else {
  firebaseApp = firebase.app(); // if already initialized, use that one
}

const db = firebaseApp.firestore();

const auth = firebase.auth();

export { db, auth };
